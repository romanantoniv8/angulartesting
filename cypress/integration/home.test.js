describe('Home Page', function () {
  beforeEach(() => {
    cy.fixture("courses.json").as("coursesjson");

    cy.server();

    cy.route("/api/courses", "@coursesjson").as("courses");

    cy.visit('/');

    cy.wait("@courses");
  });

  it('should display a list of courses', function () {
    cy.contains("All Courses");



    cy.get("mat-card").should("have.length", 9);
  });

  it('should display the advanced courses', () => {
    cy.get(".mdc-tab").should("have.length", 2);
    cy.get(".mdc-tab").last().click();
    cy.get(".mat-mdc-tab-body-active .mat-mdc-card-title").its('length').should('be.gt', 1);
    cy.get(".mat-mdc-tab-body-active .mat-mdc-card-title").first().should('contain', "Angular Security Course");
  });
});
