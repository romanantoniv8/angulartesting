import {fakeAsync, flush, flushMicrotasks, tick} from "@angular/core/testing";
import {of} from "rxjs";
import {delay} from "rxjs/operators";

describe("Async Testing Example", () => {

  it("Asynchronous test example with Jasmine Done", (done:DoneFn) => {

    let test = false;

    setTimeout(() => {

      test = true;

      expect(test).toBeTruthy();

      done();

    }, 1000);

  });

  it('this is an async test with setTimeout', fakeAsync(() => {
    let test = false;

    setTimeout(() => {

      test = true;

      expect(test).toBeTruthy();

    } ,1000);

    setTimeout(() => {

      test = false;

      expect(test).toBeFalsy();

    } ,1100);

    flush();
  }));

  it("Async test example with Promise", fakeAsync(() => {
    let test = false;

    console.log('Creating Promise');

    Promise.resolve().then(() => {
      console.log('Promise Resolved')
      test = true;
    });

    console.log('Running Test Assertions');

    flushMicrotasks();

    expect(test).toBeTruthy();

  }));

  it("Async test example - Promises + setTimeout", fakeAsync(() => {

    let counter = 0;

    Promise.resolve().then(() => {
      counter += 10;
      setTimeout(() => {
        counter += 1;
      }, 1000);
    });

    expect(counter).toBe(0);

    flushMicrotasks();

    expect(counter).toBe(10);

    flush();

    expect(counter).toBe(11);
  }));

  it("Async test - Observables", fakeAsync(() => {
    let test = false;

    const test$ = of(test).pipe(delay(1000));

    test$.subscribe(() => {
      test = true;
    });

    tick(1000);

    expect(test).toBeTruthy();
  }));
});

